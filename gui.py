import numpy as np
import os
import pickle
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread
from PyQt5.QtWidgets import (QApplication, QCheckBox, QFileDialog, QFrame,
                             QGridLayout, QHBoxLayout, QLabel, QLineEdit,
                             QMainWindow, QMessageBox, QPushButton,
                             QRadioButton, QScrollArea, QStatusBar,
                             QVBoxLayout, QWidget)
from queue import Queue
import string
import subprocess
import sys
import tempfile

nRows = 50
posLabels = ['C', '1B', '2B', '3B', 'SS', 'LF', 'CF', 'RF', 'SL', 'SR']

class runLineupFinding(QThread):
    """
    Create a QThread for running the lineup finding.

    This class allows for running the lineup finding script while
    simultaneously keeping the lineupForm GUI active.
    """

    iterated = pyqtSignal(str)
    finished = pyqtSignal()

    def __init__(self, execPath, pyneupPath, namesPath, prefsPath, nIters):
        QThread.__init__(self)
        self.execPath = execPath
        self.pyneupPath = pyneupPath
        self.namesPath = namesPath
        self.prefsPath = prefsPath
        self.nIters = nIters

    def __del__(self):
        self.wait()

    def run(self):
        """
        Run the lineup finding in a subprocess and cleanup temporary files.
        """

        self.ps = subprocess.Popen([self.execPath, self.pyneupPath,
                                    self.namesPath, self.prefsPath,
                                    self.nIters],
                                   stderr=subprocess.PIPE,
                                   universal_newlines=True)
        for line in self.ps.stderr:
            self.iterated.emit(line)
        self.ps.stderr.close()
        self.ps.wait()

        os.remove(self.namesPath)
        os.remove(self.prefsPath)

        self.finished.emit()

class mainWindow(QMainWindow):
    """
    Create the main window for status bar, menu, and toolbar functionality.
    """

    def __init__(self, parent=None):
        super().__init__()

        self.mainWidget = lineupForm(self)
        self.setCentralWidget(self.mainWidget)

        self.init_UI()

    def init_UI(self):
        """
        Initialize aspects of the main window and show it.
        """

        self.setWindowTitle('Pyneup')
        self.statusBar().showMessage('')

        self.show()

    @pyqtSlot(str)
    def updateProgress(self, text):
        """
        Update text of lineup finding in status bar.
        """

        self.statusBar().showMessage(text)

    def closeEvent(self, event):
        """
        Ask if ready to quit main window.
        """

        msg = 'Are you sure you want to quit?'
        ans = QMessageBox.question(self, 'Last Chance', msg,
                                   QMessageBox.Yes | QMessageBox.No,
                                   QMessageBox.No)

        if ans == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()

class lineupForm(QWidget):
    """
    Create a lineupForm instance.

    This class is the customized widget that makes up the lineup form GUI.
    """

    def __init__(self, parent):
        super(lineupForm, self).__init__(parent)
        self.parent = parent

        self.initUI()

    def initUI(self):
        """
        Initialize all interactive widgets and organize them into the form.
        """

        # create grid layout for rows of players
        self.plyrGridBox = QGridLayout()

        # create header row
        self.plyrGridBox.addWidget(QLabel('Player name'), 0, 2)
        self.plyrGridBox.addWidget(QLabel('Gender'), 0, 3)
        self.plyrGridBox.addWidget(QLabel('Position preferences'), 0, 4)

        self.nPlayers = 1
        self._createPlayers()

        # create button to add players
        addPlyrBtn = QPushButton('Add player')
        addPlyrBtn.clicked.connect(self.addPlayer)
        self.plyrGridBox.addWidget(addPlyrBtn, 51, 2, 1, 1)

        # add the player grid layout to a container frame
        self.plyrGridFrame = QFrame()
        self.plyrGridFrame.setLayout(self.plyrGridBox)

        # make the grid of player rows vertically scrollable
        scrollArea = QScrollArea()
        scrollArea.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        scrollArea.setWidgetResizable(True)
        scrollArea.setWidget(self.plyrGridFrame)
        minWidth = self.plyrGridFrame.sizeHint().width() \
                   + scrollArea.verticalScrollBar().sizeHint().width()
        scrollArea.setMinimumWidth(minWidth)

        # create grid layout for lineup finding parameters
        self.findParmGridBox = QGridLayout()

        # create input for number of lineup finding iterations
        self.findParmGridBox.addWidget(QLabel('Number of iterations'), 0, 0)
        self.findParmGridBox.addWidget(QLineEdit('500'), 1, 0)

        # add the lineup finding parameters layout to a container frame
        self.findParmFrame = QFrame()
        self.findParmFrame.setLayout(self.findParmGridBox)

        # create grid layout for form-wide action buttons
        self.actBtnGridBox = QGridLayout()

        # create button to clear inputs
        clearInputsBtn = QPushButton('Clear inputs')
        clearInputsBtn.clicked.connect(self.clearInputs)
        self.actBtnGridBox.addWidget(clearInputsBtn, 0, 1)

        # create button to save inputs
        saveInputsBtn = QPushButton('Save inputs')
        saveInputsBtn.clicked.connect(self.saveInputs)
        self.actBtnGridBox.addWidget(saveInputsBtn, 1, 0)

        # create button to load inputs
        loadInputsBtn = QPushButton('Load inputs')
        loadInputsBtn.clicked.connect(self.loadInputs)
        self.actBtnGridBox.addWidget(loadInputsBtn, 1, 1)

        # create button to run the lineup finding
        findLineupBtn = QPushButton('Find lineup')
        findLineupBtn.clicked.connect(self.findLineupStartStop)
        self.actBtnGridBox.addWidget(findLineupBtn, 2, 0, 1, 2)

        # add the action button grid layout to a container frame
        actBtnFrame = QFrame()
        actBtnFrame.setLayout(self.actBtnGridBox)

        # create horizontal layout for finding parameters and action buttons
        hBox = QHBoxLayout()
        hBox.addWidget(self.findParmFrame)
        hBox.addStretch(1)
        hBox.addWidget(actBtnFrame)

        # add finding parameters and action buttons layout to container frame
        findAndActFrame = QFrame()
        findAndActFrame.setLayout(hBox)

        # create top-level layout that holds player and action button grids
        vBox = QVBoxLayout()
        vBox.addWidget(scrollArea)
        vBox.addWidget(findAndActFrame)

        self.setLayout(vBox)

        self.show()

    def _createPlayers(self):
        """
        Build up rows of widgets for each player.

        This should be run by initUI to actually create the instances of all
        the widgets that make up a player row in the form. 50 rows are made
        to accomodate any roster we may have but all except for the first is
        hidden at first. "Adding" and "removing" players simply shows and hides
        the rows made here.
        """

        for i in range(nRows):
            # create button to remove player row
            removePlyrBtn = QPushButton('X')
            removePlyrBtn.clicked.connect(self.removePlayer)
            if i == 0:
                removePlyrBtn.setEnabled(False)
            self.plyrGridBox.addWidget(removePlyrBtn, i+1, 0)

            # create label for row number
            rowNumberLabel = QLabel(str(i+1))
            self.plyrGridBox.addWidget(rowNumberLabel, i+1, 1)

            # create text box for player names
            plyrNameField = QLineEdit()
            self.plyrGridBox.addWidget(plyrNameField, i+1, 2)

            # create buttons to specify player gender
            genRowLayout = QHBoxLayout()
            mRadBtn = QRadioButton('M')
            fRadBtn = QRadioButton('F')
            mRadBtn.setChecked(True)
            genRowLayout.addWidget(mRadBtn)
            genRowLayout.addWidget(fRadBtn)
            genRowFrame = QFrame()
            genRowFrame.setLayout(genRowLayout)
            self.plyrGridBox.addWidget(genRowFrame, i+1, 3, 1, 1)

            # create buttons to specify player position preferences
            posPrefRowLayout = QHBoxLayout()
            for position in posLabels:
                posPrefRowLayout.addWidget(QCheckBox(position))
            posPrefRowFrame = QFrame()
            posPrefRowFrame.setLayout(posPrefRowLayout)
            self.plyrGridBox.addWidget(posPrefRowFrame, i+1, 4, 1, 1)

            if i != 0:
                removePlyrBtn.hide()
                rowNumberLabel.hide()
                plyrNameField.hide()
                genRowFrame.hide()
                posPrefRowFrame.hide()

    def addPlayer(self):
        """
        Add a player row to the form.

        All player rows are created when the lineupForm object is initialized
        so this simply makes a new row visible.
        """

        self.nPlayers += 1
        firstInd = 8 + 5*(self.nPlayers-2)
        self.plyrGridBox.itemAt(firstInd).widget().show()
        self.plyrGridBox.itemAt(firstInd+1).widget().show()
        self.plyrGridBox.itemAt(firstInd+2).widget().show()
        self.plyrGridBox.itemAt(firstInd+3).widget().show()
        self.plyrGridBox.itemAt(firstInd+4).widget().show()

        # enable 1st remove player button if adding 2nd player now
        if self.nPlayers == 2:
            rmPlyrBtn = self.plyrGridBox.itemAt(3).widget()
            rmPlyrBtn.setEnabled(True)

        self.repaint()

    def _replaceRowWithNext(self, rowInd):
        """
        Replace the contents of the given row with those from the one below.

        To get the effect of removing a row, the contents of the row below are
        placed into the row being removed which effectively shifts the contents
        up and overwrites the row being deleted. To keep things tidy, this also
        sets the row below to the defaults so it is effectively empty.
        """

        # grab objects making up the row to delete and the one below
        rmPlyrBtn = self.plyrGridBox.itemAt(rowInd).widget()
        nxtRmPlyrBtn = self.plyrGridBox.itemAt(rowInd+5).widget()
        rowNumberLabel = self.plyrGridBox.itemAt(rowInd+1).widget()
        nxtRowNumberLabel = self.plyrGridBox.itemAt(rowInd+6).widget()
        plyrName = self.plyrGridBox.itemAt(rowInd+2).widget()
        nxtPlyrName = self.plyrGridBox.itemAt(rowInd+7).widget()
        genderFrame = self.plyrGridBox.itemAt(rowInd+3).widget()
        nxtGenderFrame = self.plyrGridBox.itemAt(rowInd+8).widget()
        plyrPrfsFrame = self.plyrGridBox.itemAt(rowInd+4).widget()
        nxtPlyrPrfsFrame = self.plyrGridBox.itemAt(rowInd+9).widget()

        # copy contents of row below into row being deleted
        plyrName.setText(nxtPlyrName.text())
        genderBtns = genderFrame.findChildren(QRadioButton)
        nxtGenderBtns = nxtGenderFrame.findChildren(QRadioButton)
        genderBtns[0].setChecked(nxtGenderBtns[0].isChecked())
        genderBtns[1].setChecked(nxtGenderBtns[1].isChecked())
        plyrPrfsBtns = plyrPrfsFrame.findChildren(QCheckBox)
        nxtPlyrPrfsBtns = nxtPlyrPrfsFrame.findChildren(QCheckBox)
        for i in range(len(plyrPrfsBtns)):
            currBtn = plyrPrfsBtns[i]
            nextBtn = nxtPlyrPrfsBtns[i]
            currBtn.setChecked(nextBtn.isChecked())

        # set row below to defaults
        nxtPlyrName.setText('')
        nxtGenderBtns[0].setChecked(True)
        nxtGenderBtns[1].setChecked(False)
        for i in range(len(nxtPlyrPrfsBtns)):
            nxtPlyrPrfsBtns[i].setChecked(False)

    def _removePlayer(self, rmPlyrBtn):
        """
        Remove the player row associated with the given remove button.

        This handles running _replaceRowWithNext from the correct row and the
        appropriate number of times to remove the specified player row. This
        also handles hiding the empty row left after doing the shifting. It is
        separate here from the removePlayer method so that the row can be
        identified by the button pressed in removePlayer but this method can
        also be used internally by the object.
        """

        # grab objects making up the player row
        rmPlyrBtnInd = self.plyrGridBox.indexOf(rmPlyrBtn)
        rowNumberLabel = self.plyrGridBox.itemAt(rmPlyrBtnInd+1).widget()
        plyrName = self.plyrGridBox.itemAt(rmPlyrBtnInd+2).widget()
        genderFrame = self.plyrGridBox.itemAt(rmPlyrBtnInd+3).widget()
        plyrPrfsFrame = self.plyrGridBox.itemAt(rmPlyrBtnInd+4).widget()

        # shift rows up starting below the row being removed
        for i in range(nRows-self.nPlayers):
            rowInd = rmPlyrBtnInd + 5*i
            self._replaceRowWithNext(rowInd)

        # hide the empty player row
        rowInd = 5*self.nPlayers - 2
        rmPlyrBtn = self.plyrGridBox.itemAt(rowInd).widget()
        rowNumberLabel = self.plyrGridBox.itemAt(rowInd+1).widget()
        plyrName = self.plyrGridBox.itemAt(rowInd+2).widget()
        genderFrame = self.plyrGridBox.itemAt(rowInd+3).widget()
        plyrPrfsFrame = self.plyrGridBox.itemAt(rowInd+4).widget()
        rmPlyrBtn.hide()
        rowNumberLabel.hide()
        plyrName.hide()
        genderFrame.hide()
        plyrPrfsFrame.hide()

        self.nPlayers -= 1

        # disable remove player button if only one left
        if self.nPlayers == 1:
            rmPlyrBtn = self.plyrGridBox.itemAt(3).widget()
            rmPlyrBtn.setEnabled(False)

    def removePlayer(self):
        """
        Remove the player row associated with the button clicked.

        This is here for catching which button was pressed so the row to be
        removed can be determined. The actual player removal is handled by
        _removePlayer.
        """

        rmPlyrBtn = self.sender()
        self._removePlayer(rmPlyrBtn)

    def _getInputs(self):
        """
        Return inputs of all visible input widgets.
        """

        allInputs = dict()
        for i in range(self.nPlayers):
            rowInputs = list()
            plyrName = self.plyrGridBox.itemAt((i*5)+5).widget()
            genderFrame = self.plyrGridBox.itemAt((i*5)+6).widget()
            genderBtns = genderFrame.findChildren(QRadioButton)
            plyrPrfsFrame = self.plyrGridBox.itemAt((i*5)+7).widget()
            plyrPrfsBtns = plyrPrfsFrame.findChildren(QCheckBox)

            plyrInfo = list()
            for j in range(len(plyrPrfsBtns)):
                if plyrPrfsBtns[j].isChecked():
                    plyrInfo.append(j)
            if genderBtns[0].isChecked():
                plyrInfo.append(-1)
            else:
                plyrInfo.append(-2)

            allInputs[plyrName.text()] = np.array(plyrInfo)

        return allInputs

    def clearInputs(self):
        """
        Remove all player rows and leave the 1st row empty.
        """

        while self.nPlayers > 0:
            rowInd = 5*self.nPlayers - 2
            rmPlyrBtn = self.plyrGridBox.itemAt(rowInd).widget()
            self._removePlayer(rmPlyrBtn)
        self.addPlayer()

    def saveInputs(self):
        """
        Ask for a save file name and write the form contents to a pickle there.
        """

        name = QFileDialog.getSaveFileName(self)
        if name[0] != '':
            inputs = self._getInputs()
            with open(name[0], 'wb') as f:
                pickle.dump(inputs, f, pickle.HIGHEST_PROTOCOL)

    def loadInputs(self):
        """
        Ask for a load file name and fill form with loaded inputs.
        """

        name = QFileDialog.getOpenFileName(self)
        if name[0] != '':
            try:
                with open(name[0], 'rb') as f:
                    inputs = pickle.load(f)
            # gracefully handle pickle loading failing
            except:
                msg = 'Could not load a roster from the selected file.'
                QMessageBox.about(self, 'Error', msg)
            # loading pickle did not throw any exceptions
            else:
                self.clearInputs()
                for i,name in enumerate(inputs):
                    ind = (i*5)+5
                    plyrName = self.plyrGridBox.itemAt(ind).widget()
                    genderFrame = self.plyrGridBox.itemAt(ind+1).widget()
                    genderBtns = genderFrame.findChildren(QRadioButton)
                    plyrPrfsFrame = self.plyrGridBox.itemAt(ind+2).widget()
                    plyrPrfsBtns = plyrPrfsFrame.findChildren(QCheckBox)

                    plyrName.setText(name)
                    if inputs[name][-1] == -1:
                        genderBtns[0].setChecked(True)
                        genderBtns[1].setChecked(False)
                    elif inputs[name][-1] == -2:
                        genderBtns[0].setChecked(False)
                        genderBtns[1].setChecked(True)
                    for j in range(10):
                        if j in inputs[name]:
                            plyrPrfsBtns[j].setChecked(True)
                        else:
                            plyrPrfsBtns[j].setChecked(False)

                    if i > 0:
                        self.addPlayer()

    def setFormPreFinding(self):
        startStopBtn = self.actBtnGridBox.itemAtPosition(2, 1).widget()
        startStopBtn.setText('Stop lineup search')

        self.plyrGridFrame.setEnabled(False)
        self.findParmFrame.setEnabled(False)
        self.actBtnGridBox.itemAtPosition(0, 1).widget().setEnabled(False)
        self.actBtnGridBox.itemAtPosition(1, 0).widget().setEnabled(False)
        self.actBtnGridBox.itemAtPosition(1, 1).widget().setEnabled(False)

    @pyqtSlot()
    def setFormPostFinding(self):
        startStopBtn = self.actBtnGridBox.itemAtPosition(2, 1).widget()
        startStopBtn.setText('Find lineup')

        self.plyrGridFrame.setEnabled(True)
        self.findParmFrame.setEnabled(True)
        self.actBtnGridBox.itemAtPosition(0, 1).widget().setEnabled(True)
        self.actBtnGridBox.itemAtPosition(1, 0).widget().setEnabled(True)
        self.actBtnGridBox.itemAtPosition(1, 1).widget().setEnabled(True)

    def findLineupStartStop(self):
        """
        Start lineup finding with the form inputs or stop it before completion.
        """

        # start running lineup finder
        if self.sender().text() == 'Find lineup':
            nIters = self.findParmGridBox.itemAtPosition(1, 0).widget()
            nIters = nIters.text()
            badChar = False
            for char in nIters:
               if char not in string.digits:
                   badChar = True
                   break
            if badChar:
                msg = 'Number of iterations must contain only digits.'
                self.parent.statusBar().showMessage(msg)
                self.repaint()
            elif int(nIters) == 0:
                msg = 'Number of iterations must be greater than zero.'
                self.parent.statusBar().showMessage(msg)
                self.repaint()
            else:
                repoDir = os.path.realpath(__file__)
                repoDir = os.path.dirname(repoDir) + '/'

                guiInputs = self._getInputs()

                plyrNames = list()
                for name in guiInputs:
                    plyrNames += name + '\n'
                plyrNamesFile = tempfile.mkstemp(dir=repoDir)
                with open(plyrNamesFile[1], 'w') as f:
                    f.writelines(plyrNames)

                prefsFile = tempfile.mkstemp(dir=repoDir)
                with open(prefsFile[1], 'wb') as f:
                    pickle.dump(guiInputs, f, pickle.HIGHEST_PROTOCOL)

                self.pyneupThread = runLineupFinding(sys.executable,
                                                     repoDir+'pyneup.py',
                                                     plyrNamesFile[1],
                                                     prefsFile[1],
                                                     nIters)
                self.pyneupThread.iterated.connect(self.parent.updateProgress)
                self.pyneupThread.finished.connect(self.setFormPostFinding)
                self.pyneupThread.start()

                self.setFormPreFinding()
                self.sender().setText('Stop lineup search')
                self.parent.statusBar().showMessage('Starting finder...')
                self.repaint()
        # abort lineup finding early
        else:
            self.pyneupThread.ps.terminate()
            self.pyneupThread.terminate()

            os.remove(self.pyneupThread.namesPath)
            os.remove(self.pyneupThread.prefsPath)

            self.setFormPostFinding()
            self.sender().setText('Find lineup')
            self.parent.statusBar().showMessage('')
            self.repaint()


if __name__ == '__main__':
    app = QApplication([])
    form = mainWindow()
    sys.exit(app.exec_())
