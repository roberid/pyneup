import pickle
import numpy as np

def save_obj(obj, name ):
    with open('database/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

##############

positions = {}

positions['Abbey'] = np.array([0,2,7,8,9,-2])
positions['Ben'] = np.array([2,3,4,5,-1])
positions['Nate'] = np.array([1,2,3,5,7,-1])
positions['Ashley'] = np.array([2,8,9,-2])
positions['Jason'] = np.array([1,2,5,7,8,9,-1])
positions['MattR'] = np.array([3,5,6,-1])
positions['Sara'] = np.array([0,2,8,9,-2])
positions['Ian'] = np.array([3,4,5,6,-1])
positions['MikeM'] = np.array([2,3,5,6,-1])
positions['Lili'] = np.array([0,2,7,8,9,-2])
positions['MikeT'] = np.array([1,2,3,5,6,-1])
positions['Hao'] = np.array([0,2,7,8,9,-1])
positions['Carmen'] = np.array([0,2,8,9,-2])
positions['Joey'] = np.array([3,4,5,6,-1])

save_obj(positions, 'positions_db')
